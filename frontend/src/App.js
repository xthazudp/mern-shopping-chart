import './App.css';
import { useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// screens
import HomeScreen from './screens/HomeScreen/HomeScreen';
import ProductScreen from './screens/ProductScreen/ProductScreen';
import CartScreen from './screens/CartScreen/CartScreen';

// components
import Navbar from './components/Navbar/Navbar';
import Backdrop from './components/Backdrop/Backdrop';
import SideDrawer from './components/SideDrawer/SideDrawer';

function App() {
  const [sideToggle, setSideToggle] = useState(false);

  return (
    <Router>
      {/* Navbr */}
      <Navbar
        click={() => {
          setSideToggle(true);
        }}
      />

      {/* SideDrawer */}
      <SideDrawer
        show={sideToggle}
        click={() => {
          setSideToggle(false);
        }}
      />

      {/* Backdrop */}
      <Backdrop
        show={sideToggle}
        click={() => {
          setSideToggle(false);
        }}
      />

      <main className="app">
        <Switch>
          <Route exact path="/" component={HomeScreen} />
          <Route exact path="/product/:id" component={ProductScreen} />
          <Route exact path="/cart" component={CartScreen} />
        </Switch>
      </main>
    </Router>
  );
}

export default App;
